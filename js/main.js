'use strict';
(function($) {
   var userHasScrolled = false,

       $headerTop = $('.header-top'),
       $mainNavigation = $('.navigation a'),
       $hamburger = $('.hamburger'),
       $navContent = $('.nav-content'),
       navopen = false,
       $h1Tag = $('.header-logo h1');

    window.onscroll = function(e) {
        // called when the window is scrolled.  
      if(detectMobileDevices(769)  === false){
        var scroll = $(window).scrollTop();
      
        if (scroll > 10) {
            userHasScrolled = true;
            $headerTop.css('background-color', 'rgba(255,255,255,1)');
          //  $headerTop.css('padding', '10px 0px');
            $h1Tag.css('background','url(../img/logo_black.png) no-repeat');
             $mainNavigation.css('color','#333333');
             $hamburger.css('color','#333333');
        } else {
            userHasScrolled = false;
            $headerTop.css('background-color', 'transparent');
           // $headerTop.css('padding', '28px 0px');
            $h1Tag.css('background','url(../img/logo.png) no-repeat');
            $mainNavigation.css('color','#ffffff');
             $hamburger.css('color','#ffffff');

            // $headerTop.css("transition", "all 0.5s");
        }
      }
        
    }
    $hamburger.on({

        click :function(event){
         if(detectMobileDevices(769) === true){
            

                //toggle between open and close navopen
                if(navopen === false){
                    navopen = true;
                    $navContent .animate({height: "200px"}, 500)
                }
                else{
                   navopen = false;
                   $navContent .animate({height: "0px"}, 500)
                }
            }
         
         }
    })



    function detectMobileDevices(windowSize) {
        if ($(window).width() < windowSize) {
          return true;
        } else {
          return false;
        }

  }
})(jQuery);




